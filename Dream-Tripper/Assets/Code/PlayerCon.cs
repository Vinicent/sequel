using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class PlayerCon : MonoBehaviour
    {
        public CharacterCon controllers;
        public float runSpeed = 40f;
        public float jumpSpeed = 8f;
        float horizontalMove = 0f;
        bool jump = false;
        public bool canMove = true;


        // Update is called once per frame
        void Update()
        {
            if (!canMove)
            {
            
                return;
            }
           
            else
            {
                horizontalMove = Input.GetAxis("Horizontal") * runSpeed;
            }


            if (Input.GetButtonUp("Jump"))
            {
                jump = true;
                Debug.Log("JUMP!");
            }
            
            if (Input.GetButton("Jump"))
            {
                controllers.m_JumpForce += jumpSpeed;                
            }

        }
        void FixedUpdate()
        {
            if (!canMove)
            {
                return;
            }
            // Move our character
            controllers.Move(horizontalMove * Time.fixedDeltaTime, jump);
            
            jump = false;
        }
    }
}
