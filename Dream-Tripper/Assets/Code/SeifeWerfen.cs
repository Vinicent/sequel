﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class SeifeWerfen : MonoBehaviour
    {
        public Transform firePoint;
        public GameObject firePointObject;
        public GameObject seifeMunition;
        public int munition = 20;

        
        void Update()
        {
            //Solange man die "Throw" Knopf drückt und der Spiler mehr als 0 Munition hat wird Seife geworfen
            if (Input.GetButtonDown("Fire1") && munition > 0)
            {
                Shoot();
                munition -= 1;
                SeifeAnzahl.instance.ChangeScore(munition);

            }
            else if (munition == 0)
            {
                //sorgt dafür, dass der Spieler nicht unendlich weiterwerfen kann
                firePointObject.SetActive(false);
            }
        }

        void Shoot()
        {
            //Instantiates die Seife, die geworfen wird
            Instantiate(seifeMunition, firePoint.position, firePoint.rotation);
        }
    }
}