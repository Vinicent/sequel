using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


    public class GameLoader : MonoBehaviour
    {
        
        public void CampingLV()
        {
            SceneManager.LoadScene("Camping");
        }
        
        public void MainStageLV()
        {
            SceneManager.LoadScene("MainStage");
        }
        
        public void QDanceLV()
        {
            SceneManager.LoadScene("QDance");
        }
        
        public void GoaLV()
        {
            SceneManager.LoadScene("SecondStage");
        }

        public void Menu()
        {
            SceneManager.LoadScene("Menu");
        }
        
        public void Intro()
        {
            SceneManager.LoadScene("Intro");
        }
    }

