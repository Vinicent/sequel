﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using dreamtripper;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;

    void Awake()
    {
        current = this;
    }

    public event Action ToiletPaperCollect;
    public void ToiletPaperHighscore()
    {
        if(ToiletPaperCollect != null)
        {
            ToiletPaperCollect();
        }
    }

    public event Action DoritoCollect;
    public void DoritoHealth()
    {
        if(DoritoCollect != null)
        {
            DoritoCollect();
        }
    }

    public event Action SyringeCollect;
    public void Invulnerability()
    {
        if (SyringeCollect != null)
        {
            SyringeCollect();
        }
    }

    public event Action SoapCollect;
    public void SoapAmmunition()
    {
        if (SoapCollect != null)
        {
            SoapCollect();
        }
    }

    public event Action TicketCollect;
    public void TicketKey()
    {
        if (TicketCollect != null)
        {
            TicketCollect();
        }
    }

    public event Action DamageReceived;
    public void Damaged()
    {
        if (DamageReceived != null)
        {
            DamageReceived();
        }
    }
}