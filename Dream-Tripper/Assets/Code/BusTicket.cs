﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class BusTicket : MonoBehaviour
    {
        public AudioSource pling;

        private void OnTriggerEnter2D(Collider2D other)
        {
            //player enter the item collider > give the player the ticket by setting the bool to "true", then play the sound and destroy it!
            if (other.gameObject.CompareTag("Player"))
            {
                GameEvents.current.TicketKey();
                //GameObject.Find("Player").GetComponent<PlayerMovement>().HasTicket=true;
                pling.Play();
                Destroy(this.gameObject);
            }
        }
    }

}