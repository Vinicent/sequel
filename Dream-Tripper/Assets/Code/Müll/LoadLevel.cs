using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace dreamtripper
{
    public class LoadLevel : MonoBehaviour
    {
        public void LoadScene()
        {
            SceneManager.LoadScene("Game");
        }

        public void Quit()
        {
            Application.Quit();
        }

        public void LoadDrugRoom()
        {
            SceneManager.LoadScene("DrugChoice");
        }

        public void LoadMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
