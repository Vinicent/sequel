using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public GameObject credits;
    public GameObject tutorial;
    public GameObject player;
    public GameObject canvas;
    public GameObject titleElements;



    private void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }


    public void PlayGame()
    {
        SceneManager.LoadScene("Intro");
     
  
    }

    public void Credits()
    {
        Debug.Log("Credits");
        credits.SetActive(true);
        titleElements.SetActive(false);
    }

    public void NoCredits()
    {
        credits.SetActive(false);
        titleElements.SetActive(true);
    }

    public void Tutorial()
    {
        Debug.Log("Tutorial");
        tutorial.SetActive(true);
        titleElements.SetActive(false);
    }

    public void NoTutorial()
    {
        tutorial.SetActive(false);
        titleElements.SetActive(true);
    }

    public void Quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

}
