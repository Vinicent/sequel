using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class LevelChange : MonoBehaviour
    {
        
        public GameObject background;
        
        public GameObject platforms;
        private void Awake()
        {
            if (PersistentManager.Instance.lsd)
            {
                background.SetActive(true);
            }
            else
            {
                background.SetActive(false);
            }

            if (PersistentManager.Instance.antiDepressiva)
            {
                platforms.SetActive(true);
            }
            else
            {
                platforms.SetActive(false);
            }
        }
    }
}
