using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class Clouds : MonoBehaviour
    {

        public float speed = 300f;
        public Vector3 startposition;
        public Vector3 direction = new Vector3(-1, 0, 0);
        public GameObject reset;

        void Update()
        {
            this.transform.position = this.transform.position + direction * speed * Time.deltaTime;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {

            if (collision.gameObject.CompareTag("Reset"))
            {
                Die();
            }
        }


        private void Die()
        {
            Destroy(this.gameObject);
        }

    }



}
