using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dreamtripper
{
    public class SceneStart : MonoBehaviour
    {
        public Toggle weed;
        public Toggle lsd;
        public Toggle hero;
        public Toggle anti;

        private void Start()
        {
            if (PersistentManager.Instance.weed)
            {
                weed.isOn = true;
            }
            else
            {
                weed.isOn = false;
            }
            if (PersistentManager.Instance.lsd)
            {
                lsd.isOn = true;
            }
            else
            {
                lsd.isOn = false;
            }
            if (PersistentManager.Instance.heroin)
            {
                hero.isOn = true;
            }
            else
            {
                hero.isOn = false;
            }
            if (PersistentManager.Instance.antiDepressiva)
            {
                anti.isOn = true;
            }
            else
            {
                anti.isOn = false;
            }
        }


        public void Weed()
        {
            if (!PersistentManager.Instance.weed)
            {
                PersistentManager.Instance.weed = true;
            }
            else
            {
                PersistentManager.Instance.weed = false;
            }
        }

        public void LSD()
        {
            if (!PersistentManager.Instance.lsd)
            {
                PersistentManager.Instance.lsd = true;
            }
            else
            {
                PersistentManager.Instance.lsd = false;
            }
        }

        public void Anti()
        {
            if (!PersistentManager.Instance.antiDepressiva)
            {
                PersistentManager.Instance.antiDepressiva = true;
            }
            else
            {
                PersistentManager.Instance.antiDepressiva = false;
            }
        }

        public void Hero()
        {
            if (!PersistentManager.Instance.heroin)
            {
                PersistentManager.Instance.heroin = true;
            }
            else
            {
                PersistentManager.Instance.heroin = false;
            }
        }
    }
}
