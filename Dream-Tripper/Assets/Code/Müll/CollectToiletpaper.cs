﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace dreamtripper
{
    public class CollectToiletpaper : MonoBehaviour
    {
        public AudioSource ploppSound;
        
        [SerializeField]
        //Checks for player tag and adds points to the Highscore + playing a soundeffect

        private void OnTriggerEnter2D(Collider2D other)
        {
            if(CompareTag("Player"))
            {
                ploppSound.Play();
                Destroy(this.gameObject);
                GameEvents.current.ToiletPaperHighscore();
            }
        }
    }

}
