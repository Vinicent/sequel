using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class Enemy : MonoBehaviour
    {
        public GameObject destroyObject;
        private GameObject player;

        public float flySpeed;
        public AudioSource crash;
        private bool m_FacingRight = true;

        public Vector2 pointA;
        public Vector2 pointB;
        public float time;
        private void Awake()
        {
            player = GameObject.Find("Player");

        }
        private void Update()
        {
            time = Mathf.PingPong(Time.time * flySpeed, 1);
            transform.position = Vector2.Lerp(pointA, pointB, time);
            //transform.rotation = Quaternion.Lerp(0f, 180, 0, time);

            if (time > 0.99f && !m_FacingRight)
            {
                Flip();
            }
            if (time < 0.01f && m_FacingRight)
            {
                Flip();
            }
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                GameEvents.current.Damaged();
                crash.Play();
            }

        }
        public void TakeDamage()
        {
                Die();
        }

        private void Flip()
        {
            m_FacingRight = !m_FacingRight;

            transform.Rotate(new Vector2(0f, 180f));
        }

        //Deaktiviert den Collider der Damage verursacht, zerstört ein Gameobjekt und  erhöht den Highscore
        void Die()
        {
            // Der Collider hängt am selben GameObject 
            Collider2D collider = GetComponent<Collider2D>();
            //bei manchen Gegnern hängt er jedoch am child
            if (collider == null)
            {
                collider = GetComponentInChildren<Collider2D>();
            }

            collider.enabled = false;
            destroyObject.SetActive(false);
        }
    }
}
