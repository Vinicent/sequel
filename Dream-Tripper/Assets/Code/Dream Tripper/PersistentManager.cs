using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dreamtripper
{
    public class PersistentManager : MonoBehaviour
    {
        public static PersistentManager Instance { get; private set; }

        public bool weed = false;
        public bool lsd = false;
        public bool antiDepressiva = false;
        public bool heroin = false;

        public GameObject weedToggle;
        public GameObject lsdToggle;
        public GameObject antiToggle;
        public GameObject heroToggle;


        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        private void Update()
        {
            weedToggle = GameObject.FindGameObjectWithTag("Weed");
            lsdToggle = GameObject.FindGameObjectWithTag("LSD");
            antiToggle= GameObject.FindGameObjectWithTag("AntiDepri");
            heroToggle = GameObject.FindGameObjectWithTag("Hero");
        }
    }
}