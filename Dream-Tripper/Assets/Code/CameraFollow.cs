using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace dreamtripper
{ 
    public class CameraFollow : MonoBehaviour
    {
        public Transform target;
        public float smoothTime = 0.3F;
        private Vector3 velocity = Vector3.zero;
        public int xWert;
        public int ywert;
     
        void Update()
        {
            // Define a target position above and behind the target transform
            Vector2 targetPosition = target.TransformPoint(new Vector2(xWert, ywert));
     
            // Smoothly move the camera towards that target position
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        }
    }
}
